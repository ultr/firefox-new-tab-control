all: clean
	jpm xpi

clean:
	rm -f *.xpi

run:
	jpm run -b `which iceweasel &> /dev/null && echo iceweasel || echo firefox`
