const { Cc, Ci, Cu, CC } = require('chrome');

Cu.import('resource://gre/modules/Services.jsm');

const ViewCore           = require('sdk/view/core');
const HotKeys            = require('sdk/hotkeys');
const Mod                = require('sdk/content/mod');
const PreferencesService = require('sdk/preferences/service');
const PrefsTarget        = require('sdk/preferences/event-target').PrefsTarget;
const SimplePrefs        = require('sdk/simple-prefs');
const Style              = require('sdk/stylesheet/style').Style;
const Tabs               = require('sdk/tabs');
const TabsUtils          = require('sdk/tabs/utils');
const Windows            = require('sdk/windows');

const { setTimeout, clearTimeout } = require("sdk/timers");

const TabBrowserProperties      = Services.strings.createBundle('chrome://browser/locale/tabbrowser.properties');
const BrowserStartupPrefsTarget = PrefsTarget({ branchName: 'browser.startup.' });
const AboutNewTabService        = Cc['@mozilla.org/browser/aboutnewtab-service;1'].getService(Ci.nsIAboutNewTabService);

const ENABLE_TAB_POSITIONING_TIMER_INTERVAL = 30;  // ms
const SCROLL_SWITCH_ACTIVE_TAB_TIMEOUT      = 300; // ms

let DefaultNewTabHotkey       = null;  // hotkey for opening new tab in a default position
let TabPositioningDisabled    = false; // indicetes tab positioning and grouping is temporarly disabled
let TabPositioningEnableTimer = null;  // timer for automatic reenabling of tab positioning and grouping
let GroupedTabs               = [];    // array for storing tabs that belong to the current group
let RecentTab                 = null;  // keeps track of last recently active tab
let CurrentWindow             = null;  // keeps track of currently active Browser Window
let ScrollSwitchActiveTab     = null;  // active tab object used for scroll tab switching

const disableTabPositioning = function disableTabPositioning() {
    // temporarly disable tab positioning
    TabPositioningDisabled = true;
    if (TabPositioningEnableTimer) {
        clearTimeout(TabPositioningEnableTimer);
        TabPositioningEnableTimer = null;
    }
    TabPositioningEnableTimer = setTimeout(enableTabPositioning, ENABLE_TAB_POSITIONING_TIMER_INTERVAL);
};

const enableTabPositioning = function enableTabPositioning() {
    // reenable tab positioning
    TabPositioningDisabled = false;
    if (TabPositioningEnableTimer) {
        clearTimeout(TabPositioningEnableTimer);
        TabPositioningEnableTimer = null;
    }
};

const moveTabTo = function moveTabTo(tab, index, ignoreIfNotAfter) {
    const tabView = ViewCore.viewFor(tab);
    const tabBrowser = TabsUtils.getTabBrowserForTab(tabView);
    // ignore tabs that are positioned before the active tab, required for 'Undo Close Tab'
    if (!ignoreIfNotAfter || tab.index > Tabs.activeTab.index) {
        // request moving the tab
        tabBrowser.moveTabTo(tabView, index);
        // delayed move, because Firefox may do late tab moving too
        setTimeout(
            function(){
                // ignore tabs that are positioned before the active tab, required for 'Undo Close Tab'
                if (!ignoreIfNotAfter || tab.index > Tabs.activeTab.index) {
                    // request moving the tab
                    tabBrowser.moveTabTo(tabView, index);
                }
            },
            0
        );
    }
};

const random = function random(from, to) {
    // generate a random number within [from,to] inclusive
    return from + Math.floor(Math.random() * (to - from + 1));
};

const randomHue = function randomHue() {
    // hue ranges array with weights
    let ranges = [];
    let weights = 0;
    // create color ranges with weights to draw from
    const entries = SimplePrefs.prefs.highlightColors.split(/ *, */);
    for (let entry of entries) {
        let [from, to] = entry.split('-');
        if (from === undefined || from === '') {
            continue;
        }
        if (to === undefined || to === '') {
            to = from;
        }
        from = +from;
        to   = +to;
        if (
            isNaN(from) || isNaN(to)      ||
            from < 0    || from > 360 + 2 ||
            to   < 0    || to   > 360 + 2 ||
            from > to
        ) {
            continue;
        }
        weights += to - from + 1;
        ranges.push({order: weights, from: from, to: to});
    }
    // if no valid ranges, use all colors
    if (ranges.length === 0) {
        weights = 361;
        ranges.push({order: weights, from: 0, to: 360});
    }
    // get random order
    const order = random(1, weights);
    // find the hue range by order
    let i = -1;
    let o = 0;
    while (o < order) {
        i++;
        o = ranges[i].order;
    }
    const range = ranges[i];
    // get the hue for given order
    const hue = range.to - (range.order - order);
    return hue;
};

const clearTabHighlight = function clearTabHighlight(tab) {
    const tabView = ViewCore.viewFor(tab);
    // check if the tab is highlighted
    if (tabView._NewTabControlHighlightData) {
        // remove tab's CSS style
        tabView.classList.remove(tabView._NewTabControlHighlightData.cssClass);
        Mod.detachFrom(tabView._NewTabControlHighlightData.cssStyle, tabView.ownerGlobal);
        tabView._NewTabControlHighlightData = null;
    }
};

const setTabHighlight = function setTabHighlight(tab, hue) {
    // clear previous highlight
    clearTabHighlight(tab);
    // create CSS style
    let color = 'hsla(' + hue + ',100%,50%,0.5)';
    if (hue === 361) {
        color = 'rgba(255,255,255,0.5)';
    } else if(hue === 362) {
        color = 'rgba(0,0,0,0.5)';
    }
    const cssClass = 'newtabcontrol-highlight' + tab.id;
    const cssValue = 'background-image: radial-gradient(' +
                   'ellipse 50% 80% at bottom center, ' +
                   color + ', transparent) !important;';
    const cssStyle = Style(
        { source: 'tab.tabbrowser-tab.' + cssClass + ' > *.tab-stack { ' + cssValue + ' }' }
    );
    // set the CSS style for tab
    const tabView = ViewCore.viewFor(tab);
    Mod.attachTo(cssStyle, tabView.ownerGlobal);
    tabView.classList.add(cssClass);
    // save the style object and CSS class name
    tabView._NewTabControlHighlightData = { cssClass: cssClass, cssStyle: cssStyle };
};

const addressBarForTab = function addressBarForTab(tab) {
    // get address bar element for given tab, or null if unsuccessful
    try {
        return ViewCore.viewFor(tab.window).document.getElementById('urlbar');
    } catch (e) {
        return null;
    }
};

const keepAddressBarContent = function keepAddressBarContent(tab, cleanup) {
    // set up address bar's content if it was saved before
    if (tab._AddressBarContent) {
        // get the address bar element
        const addressBar = addressBarForTab(tab);
        // set the content
        if (addressBar) {
            addressBar.value          = tab._AddressBarContent.value;
            addressBar.selectionStart = tab._AddressBarContent.selectionStart;
            addressBar.selectionEnd   = tab._AddressBarContent.selectionEnd;
        }
        // clear the saved content
        if (cleanup) {
            tab._AddressBarContent = null;
        }
    }
};

const findTab = function findTab(window, index) {
    if (index < 0 || index >= window.tabs.length) {
        return null;
    }
    for (let i = 0; i < window.tabs.length; i++) {
        const tab = window.tabs[i];
        if (tab.index === index) {
            return tab;
        }
    }
    return null;
};

const onTabClose = function onTabClose() {
    const tab = this;
    // no active tab means the Firefox is now closing
    if (Tabs.activeTab) {
        // remove the tab from the grouped tabs array
        const index = GroupedTabs.indexOf(tab);
        if (index > -1) {
            GroupedTabs.splice(index, 1);
        }
        // clear tracking of recent tab if it was closed
        if (tab === RecentTab) {
            RecentTab = null;
        }
        // if requested, activate tab to the left on closing the active tab
        if (SimplePrefs.prefs.focusLeftOnClose) {
            if (Tabs.activeTab === tab) {
                if (CurrentWindow) {
                    const leftindex = tab.index - 1;
                    const lefttab = findTab(CurrentWindow, leftindex);
                    if (lefttab) {
                        lefttab.activate();
                    }
                }
            }
        }
    }
};

const onTabOpen = function onTabOpen(tab) {
    // check if the tab is an empty new one (Ctrl+T)
    const isEmptyNewTab = (
        ( tab.readyState === 'uninitialized' || tab.readyState === 'complete' ) &&
        (
            tab.title === TabBrowserProperties.GetStringFromName('tabs.emptyTabTitle') ||
            tab.title === 'New Tab'
        )
    );
    // listen for tab close event
    tab.on('close', onTabClose);
    // do highlighting
    if (SimplePrefs.prefs.highlightUnreadTabs) {
        let hue = undefined;
        if (SimplePrefs.prefs.groupOpenedLinks && !TabPositioningDisabled) {
            hue = Tabs.activeTab._NewTabControlHighlightGroupHue;
        }
        if (hue === undefined) {
            hue = randomHue();
        }
        if (SimplePrefs.prefs.groupOpenedLinks && !TabPositioningDisabled) {
            Tabs.activeTab._NewTabControlHighlightGroupHue = hue;
        }
        // set tab highlight
        setTabHighlight(tab, hue);
    }
    // do tab processing
    if (TabPositioningDisabled) {
        // reenable temporarly disabled tab positioning
        enableTabPositioning();
    } else {
        // check new tab and preferences
        if (
            (!isEmptyNewTab && SimplePrefs.prefs.linkTabOnRight)   ||
            (!isEmptyNewTab && SimplePrefs.prefs.groupOpenedLinks) ||
            ( isEmptyNewTab && SimplePrefs.prefs.newTabOnRight )
        ) {
            // calculate new tab position
            let index = 0;
            if (tab.window.tabs.length > 0) {
                // open the tab right next to current ...
                index = Tabs.activeTab.index + 1;
                // ... or after the last tab in the group
                if (SimplePrefs.prefs.groupOpenedLinks && !isEmptyNewTab) {
                    for (let groupedTab of GroupedTabs) {
                        index = Math.max(index, groupedTab.index + 1);
                    }
                }
            }
            // move tab (even if already in this position, because Firefox may do late tab moving too)
            moveTabTo(tab, index, true);
        }
        // add the tab to the grouped tabs array
        if (isEmptyNewTab) {
            GroupedTabs.splice(0, GroupedTabs.length);
        } else {
            GroupedTabs.push(tab);
        }
    }
    // keep address bar's content
    if (SimplePrefs.prefs.keepAddressBarContent && isEmptyNewTab) {
        // get the address bar element
        let addressBar = addressBarForTab(tab);
        // check if the address bar has focus
        let activeElement = addressBar.ownerDocument.activeElement;
        let el = activeElement;
        while (el !== null) {
            if (el === addressBar) {
                break;
            }
            el = el.parentNode;
        }
        if (el) {
            // save address bar content
            tab._AddressBarContent = {
                selectionEnd:   addressBar.selectionEnd,
                selectionStart: addressBar.selectionStart,
                value:          addressBar.value
            };
            // dispatch ESC key to the address bar to reset the content
            let ev = addressBar.ownerDocument.createEvent('KeyboardEvent');
            ev.initKeyEvent('keypress', false, false, null, false, false, false, false, 27, 0);
            addressBar.dispatchEvent(ev);
            // blur the address bar element if the content is not empty
            if (addressBar.value) {
                addressBar.blur();
            }
            // schedule setting up the saved address bar content in the new tab
            setTimeout(keepAddressBarContent.bind(null, tab, false), 1);
            tab.on('ready', keepAddressBarContent.bind(null, tab, true));
        }
    }
    // focus empty new tab's content instead of its address bar
    if (SimplePrefs.prefs.focusNewTabContent && isEmptyNewTab) {
        let tabView = ViewCore.viewFor(tab);
        let browser = TabsUtils.getBrowserForTab(tabView);
        let browserView = ViewCore.viewFor(browser);
        setTimeout(browserView.focus.bind(browserView), 0);
    }
};

const onTabActivate = function onTabActivate(tab) {
    // reset the grouped tabs array
    GroupedTabs.splice(0, GroupedTabs.length);
    // clear tab highlight
    clearTabHighlight(tab);
    // save current BrowserWindow
    CurrentWindow = tab.window;
};

const onTabDeactivate = function onTabDeactivate(tab) {
    // reset the grouped tabs array
    GroupedTabs.splice(0, GroupedTabs.length);
    // keep track of last recently active tab
    RecentTab = tab;
};

const onBrowserWindowMouseClick = function onBrowserWindowMouseClick(ev) {
    // check newTabDefaultWithMouse preference
    if (SimplePrefs.prefs.newTabDefaultWithMouse) {
        // get target element
        const element = ev.target;
        if (element) {
            // temporarly disable tab positioning for given UI elements
            if (
                element.id === 'new-tab-button'  ||
                element.id === 'tabbrowser-tabs' ||
                element.id === 'privateTab-toolbar-openNewPrivateTab'
            ) {
                disableTabPositioning();
            }
        }
    }
};

const onTabBarMouseDblClick = function onTabBarMouseDblClick(ev) {
    // check newTabDefaultWithMouse preference
    if (SimplePrefs.prefs.newTabDefaultWithMouse) {
        // get target element
        const element = ev.target;
        if (element) {
            // temporarly disable tab positioning for tab bar
            if (element.id === 'tabbrowser-tabs') {
                disableTabPositioning();
            }
        }
    }
};

const onTabBarMouseDrop = function onTabBarMouseDrop(ev) {
    // temporarly disable tab positioning for tab bar
    disableTabPositioning();
};

const onTabBarMouseWheel = function onTabBarMouseWheel(ev) {
    let tabBar = this;
    // switch tab if enabled
    if (SimplePrefs.prefs.scrollSwitchesTabs) {
        if (ev.ctrlKey) {
            // with Ctrl modifier key switch to recently active tab
            if (RecentTab) {
                RecentTab.activate();
            }
        } else {
            // determine directoion
            let direction = (ev.deltaX ? ev.deltaX : ev.deltaY);
            if (direction != 0) {
                direction /= Math.abs(direction); // normalize to 1 or -1
                // find last tab index
                const lastTabIndex = Tabs.activeTab.window.tabs.length - 1;
                // calculate new active tab index
                let index = 0;
                const timestamp = (new Date()).getTime();
                if (ev.shiftKey) {
                    index = (direction === -1 ? 0 : lastTabIndex);
                } else {
                    index = Tabs.activeTab.index;
                    if (ScrollSwitchActiveTab)
                    {
                        if (timestamp <= ScrollSwitchActiveTab.timestamp + SCROLL_SWITCH_ACTIVE_TAB_TIMEOUT) {
                            index = ScrollSwitchActiveTab.index;
                        }
                    }
                    index += direction;
                    index = Math.max(index, 0);
                    index = Math.min(index, lastTabIndex);
                }
                // save activated tab index with timestamp, so that we do not rely on late-updated Tabs.activeTab
                ScrollSwitchActiveTab = {
                    index:     index,
                    timestamp: timestamp
                };
                // find tab with given position index and schedule it for activation
                const tab = findTab(Tabs.activeTab.window, index);
                if (tab) {
                    tab.activate();
                }
            }
        }
        ev.preventDefault();
    }
};

const onBrowserWindowOpen = function onBrowserWindowOpen(browserWindow) {
    // listen to close events for already opened tabs in this window
    for (let tab of browserWindow.tabs) {
        tab.on('close', onTabClose);
    }
    // listen to click events in window
    const windowView = ViewCore.viewFor(browserWindow);
    windowView.addEventListener('click', onBrowserWindowMouseClick, true);
    // listen to double-click and drop events in tab bar
    const tabBar = windowView.document.getElementById('tabbrowser-tabs');
    tabBar.addEventListener('dblclick', onTabBarMouseDblClick, true);
    tabBar.addEventListener('drop'    , onTabBarMouseDrop    , true);
    tabBar.addEventListener('wheel'   , onTabBarMouseWheel   , true);
};

const releaseBrowserWindow = function releaseBrowserWindow(browserWindow) {
    // stop listening to close events for already opened tabs in this window
    for (let tab of browserWindow.tabs) {
        tab.off('close', onTabClose);
    }
    // stop listening to click events in window
    const windowView = ViewCore.viewFor(browserWindow);
    windowView.removeEventListener('click', onBrowserWindowMouseClick, true);
    // stop listening to double-click and drop events in tab bar
    const tabBar = windowView.document.getElementById('tabbrowser-tabs');
    tabBar.removeEventListener('dblclick', onTabBarMouseDblClick, true);
    tabBar.removeEventListener('drop'    , onTabBarMouseDrop    , true);
    tabBar.removeEventListener('wheel'   , onTabBarMouseWheel   , true);
};

const openDefaultNewTab = function openDefaultNewTab() {
    // pause tab processing
    disableTabPositioning();
    // open new tab
    let url = AboutNewTabService.newTabURL || 'about:newtab';
    Tabs.open(url);
    // reset the grouped tabs array
    GroupedTabs.splice(0, GroupedTabs.length);
};

const setupNewTabURL = function setupNewTabURL() {
    // set URL to custom one or user's homepage
    let url = SimplePrefs.prefs.newTabCustomURL;
    if (!url) {
        // get homepage URL
        url = PreferencesService.getLocalized('browser.startup.homepage', 'about:home').split('|')[0];
    }
    // fix URL protocol if needed
    if (url.indexOf(':') === -1) {
        url = 'http://' + url;
    }
    // override new tab URL
    AboutNewTabService.newTabURL = url;
};

const resetNewTabURL = function resetNewTabURL() {
    // reset new tab URL to default
    AboutNewTabService.resetNewTabURL();
};

const onNewTabURLPreferenceChange = function onNewTabURLPreferenceChange() {
    // setup or reset new tab URL accordingly
    if (SimplePrefs.prefs.newTabHomepage) {
        setupNewTabURL();
    } else {
        resetNewTabURL();
    }
};

const onNewTabHomepageURLChange = function onNewTabHomepageURLChange() {
    // setup new tab URL if active
    if (SimplePrefs.prefs.newTabHomepage) {
        setupNewTabURL();
    }
};

const uninstall = function uninstall() {
    // clean up preferences settings when uninstalled
    for (let preference in SimplePrefs.prefs) {
        delete SimplePrefs.prefs[preference];
    }
};

exports.main = function main(options) {
    // listen for new browser windows being opened and process them
    for (let browserWindow of Windows.browserWindows) {
        onBrowserWindowOpen(browserWindow);
    }
    Windows.browserWindows.on('open', onBrowserWindowOpen);
    // listen for new tabs being opened, activated and deactivated
    Tabs.on('open'      , onTabOpen      );
    Tabs.on('activate'  , onTabActivate  );
    Tabs.on('deactivate', onTabDeactivate);
    // create Ctrl+Alt+T hotkey for opening empty new tab in default position
    DefaultNewTabHotkey = HotKeys.Hotkey(
        {
            combo:   'accel-alt-t',
            onPress: openDefaultNewTab,
        }
    );
    // listen to newTabHomepage and newTabCustomURL preferences changes
    SimplePrefs.on('newTabHomepage', onNewTabURLPreferenceChange);
    SimplePrefs.on('newTabCustomURL', onNewTabURLPreferenceChange);
    // listen to homepage settings changes
    BrowserStartupPrefsTarget.on('homepage', onNewTabHomepageURLChange);
    // check new tab homepage settings and setup homepage if needed
    if (SimplePrefs.prefs.newTabHomepage) {
        setupNewTabURL();
    }
};

exports.onUnload = function onUnload(reason) {
    // stop listening for new browser windows being opened and release them
    Windows.browserWindows.removeListener('open', onBrowserWindowOpen);
    for (let browserWindow of Windows.browserWindows) {
        releaseBrowserWindow(browserWindow);
    }
    // stop listening for new tabs being opened, activated and deactivated
    Tabs.removeListener('open'      , onTabOpen      );
    Tabs.removeListener('activate'  , onTabActivate  );
    Tabs.removeListener('deactivate', onTabDeactivate);
    // destroy the Ctrl+Alt+T hotkey
    if (DefaultNewTabHotkey) {
        DefaultNewTabHotkey.destroy();
    }
    // stop listening to newTabHomepage and newTabCustomURL preferences changes
    SimplePrefs.removeListener('newTabHomepage', onNewTabURLPreferenceChange);
    SimplePrefs.removeListener('newTabCustomURL', onNewTabURLPreferenceChange);
    // stop listening to homepage settings changes
    BrowserStartupPrefsTarget.removeListener('homepage', onNewTabHomepageURLChange);
    // check new tab homepage settings and reset homepage if needed
    if (SimplePrefs.prefs.newTabHomepage) {
        resetNewTabURL();
    }
    // handle uninstallation
    if (reason === 'uninstall') {
        uninstall();
    }
};
