New Tab Control
===============

This Firefox add-on allows you to control many features of newly opened tabs.

Features (see add-on preferences):

1. Links opened in a new tabs are positioned right next to the current tab. The 'browser.tabs.insertRelatedAfterCurrent' setting will be ignored.

2. Links opened from the same tab and bookmarks opened simultaneously are positioned after the last tab of their group. This also mimics the 'browser.tabs.insertRelatedAfterCurrent' behavior.

3. Empty new tabs (Ctrl+T) are opened right next to the current one. Use Ctrl+Alt+T shortcut to open a new tab at the default position.

4. Empty new tabs will be placed in default position when opened with a mouse by clicking New Tab button or double clicking the tab bar.

5. Loads your homepage or custom URL address into newly opened empty tabs.

6. Enter custom URL address to be opened in new tabs. Leave this field empty to use your homepage address.

7. Keep address bar's modified content when opening a new tab.

8. Focus the page loaded into a newly opened tab instead of its address bar. Disabled by default.

9. New tabs will be highlighted with a random colorful background until activated. For grouped tabs the same color will be used.

10. Enter comma separated custom hues (0-360, 361 for white, 362 for black) and hue ranges used for highlighting. Leave empty to use all colors.

11. Scroll wheel over the tab bar switches tabs forwards and backwards. Use Shift key to jump to the first or last tab, and Ctrl key to switch to recently active tab.

12. Closing current tab activates one on the left.

Add-on's homepage: https://addons.mozilla.org/firefox/addon/new-tab-control/


Source Code
-----------

New Tab Control is released under GPL license version 3.

You can find source code and contribute on GitLab:
https://gitlab.com/ultr/firefox-new-tab-control/

Translations and patches are welcome.


Donations
---------

If you like my add-on, please consider making a small donation.
